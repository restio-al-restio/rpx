#!/bin/sh
##
# Usage: sh autogen.sh
##

set -ex

guile-baux-tool snuggle m4 build-aux

guile-baux-tool import \
    punify \
    gbaux-do

autoreconf --verbose --force --install --symlink

# autogen.sh ends here
