dnl configure.ac
dnl Copyright (C) 2010-2021 Thien-Thi Nguyen
dnl Portions Copyright (C) 2000, 2001, 2002, 2003, 2004 Shawn Betts
dnl
dnl This file is part of rpx.
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program.  If not, see <https://www.gnu.org/licenses/>.

AC_INIT([rpx],[1.4],[ttn@gnuvola.org])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([build-aux])
AM_INIT_AUTOMAKE([1.11 -Wall no-dist-gzip dist-lzip])

# Make sure configuration doesn't auto-compile anything.
GUILE_AUTO_COMPILE=0
export GUILE_AUTO_COMPILE

AC_ARG_WITH([xterm],
  AS_HELP_STRING([--with-xterm=PROG],
    [set the x terminal emulator (default: x-terminal-emulator)]),
  [term_prog=$withval],
  [term_prog="x-terminal-emulator"])
AC_SUBST(term_prog)

GUILE_PROGS

GUILE_MODULE_REQUIRED(ttn-do zzz banalities)
GUILE_MODULE_REQUIRED(srfi srfi-1)
GUILE_MODULE_REQUIRED(srfi srfi-11)
GUILE_MODULE_REQUIRED(srfi srfi-13)
GUILE_MODULE_REQUIRED(srfi srfi-14)
GUILE_MODULE_REQUIRED(ttn-do valid-ucs-p)
GUILE_MODULE_REQUIRED(ttn-do zzz personally)
GUILE_MODULE_REQUIRED(ttn-do zzz x-protocol)
GUILE_MODULE_REQUIRED(ttn-do zzz x-kbgrunge)
GUILE_MODULE_REQUIRED(ttn-do zzz x-umbrages)
GUILE_MODULE_REQUIRED(ttn-do mogrify)
GUILE_MODULE_REQUIRED(ice-9 q)
GUILE_MODULE_REQUIRED(ice-9 and-let-star)
GUILE_MODULE_REQUIRED(ice-9 popen)
GUILE_MODULE_REQUIRED(ice-9 regex)
GUILE_MODULE_REQUIRED(ice-9 rdelim)
GUILE_MODULE_REQUIRED(ice-9 pretty-print)

dnl Some versions of Guile don't provide getsid(2).
AC_CACHE_CHECK([if Guile provides getsid(2)],[guile_cv_getsid],[
  GUILE_CHECK([guile_cv_getsid],[(exit getsid)])
  AS_IF([test 0 = $guile_cv_getsid],
    [guile_cv_getsid=yes],
    [guile_cv_getsid=no])
])
GETSID_FROM_TTN_DO=
AS_IF([test no = $guile_cv_getsid],[
  GUILE_MODULE_REQUIRED_EXPORT([(ttn-do zz sys linux-gnu)],[getsid])
  GETSID_FROM_TTN_DO='#:use-module ((ttn-do zz sys linux-gnu) #:select (getsid))'
])
AC_SUBST([GETSID_FROM_TTN_DO])

AC_CONFIG_FILES([
 Makefile
 doc/Makefile
 src/Makefile
 src/body:src/rpx.in
])

AC_OUTPUT

dnl configure.ac ends here
